# Les Amoureux du Web 💖

Bienvenue sur Les Amoureux du Web (LAW), une collection de liens pratiques et relativement bien rangés pour nous les devs.

🇬🇧 Welcome on *Les Amoureux du Web* (which means the web lovers, in french, which is a language, but of course you know that french is a language, don't you ?) a handy collection of finely selected links, quite nicely ordered, for all of us, the web developers.

**Au sommaire** :

- Newsletters indispensables
- Pédagogie
- Setup
- Git
  - Forges
  - Tuto
- Markdown
  - Obsidian
- API
- DNS
- Hébergement
- UI/UX
- Couleurs
  - CSS color 4 specs
  - palette
  - gradients
- Images et vidéos
  - Wireframes / Diagrames / Schémas
  - Maquette
  - Illustrations
  - Illustrations / objets 3D
  - AI ART
  - Icônes
  - Banques de photos et de vidéos
  - Émojis
  - Images de placeholder
  - Éditeurs d'image
  - Image optimization
- Audio
  - Audio samples download
  - Audio tools
- Video filters
- Présenter son code, communiquer, faire des sliders, etc
- Typographie
- HTML
- Accessibilité
- SEO / Social markup
- Cartographie
- Environment
- Node.js
- CSS
  - Web fonts
  - Media queries
  - Container queries
  - Unités
  - Sélecteurs
  - Masonry
  - Aspect ratio
  - Flexbox
  - Grid
  - Pseudo-classes
  - CSS Resets and Normalizers
  - CSS masks
  - HTML test
  - CSS frameworks
  - CSS animations
  - CSS animations via JS
  - Sass
- JavaScript
  - Easings
  - Mathematics
  - Datavis
  - Console.log
  - Asynchrone
  - Bookmarklet
  - Frameworks
  - User scripts
  - JS games
  - JS scripts
  - Coloration syntaxique de code, via JS
- Task runner / assets bundler
- PHP
  - PHP + MySQL
- Database
- Mobile development and PWA
- Webassembly
- Python
- Sécurité
- JSON
- CMS
- No-code
  - e-commerce
- Kanban
- Linux
- Jeux
- Électronique
- Plaisir des yeux
- Éducation au numérique
- Misc

## Newsletters indispensables

Les liens qui suivent proviennent souvent d'une pincée de newsletters vraiment chouettes, que je suis depuis des années. Donc abonnez-vous y, c'est gratos, et ça amène de la bonne inspiration dans vos boîtes mails!

- [WDRL (Web Development Reading List)](https://wdrl.info/) : front/back, Anselm Hannemann nous offre des liens et des analyses pertinentes, j'adore
- [codrops](https://tympanus.us10.list-manage.com/subscribe?u=10ffec652b0f54fe5d8682ac0&id=a9cab6dcf0) : front, j'adore
- [Frontend Focus](https://frontendfoc.us/) : front, souvent plein de bons liens

mais aussi :

- [TechTrash](https://techtrash.us16.list-manage.com/subscribe?u=0c3890af463fc84796e6a0b54&id=80c8a52fc5) en français, pour rire un peu de la "startup nation" et ses "bullshiters" (baratineurs)
- [Les "Spark" de CodePen](https://codepen.io/spark) : du front, avec de très bonnes pistes creusées par leur communauté ([inscription newsletter ici](https://codepen.io/accounts/signup/user/free))
- https://substack.com/profile/2756212-tania-rascia : Tania est une super codeuse, qui écrit de chouette tutos

ah et aussi :

- [La chaine YT de VSCode](https://www.youtube.com/c/Code/videos) qui propose des petites astuces très sympas

## Pédagogie

- [Scratch](https://scratch.mit.edu/) (MIT) Scratch est la plus grande communauté de codage pour enfants au monde et un langage de codage doté d'une interface visuelle simple qui permet aux jeunes de créer des histoires, des jeux et des animations numériques.
- [App Inventor](http://appinventor.mit.edu/) (MIT) intuitive, visual programming environment that allows everyone – even children – to build fully functional apps for Android and iOS.
- [Rety](https://rety.verou.me/) is a library that allows you to record the edits you make on one or more pieces of text (usually code) and replay them later to recreate the same typing flow.

## Setup

Tout pour tester son setup :

- [Tester sa connexion](https://www.nperf.com/fr/) (nperf.com)
- [Tester son micro](https://fr.mictests.com/check) (mictests.com)
- [Tester sa webcam](https://fr.webcamtests.com/) (webcamtests.com)

## Git

### Forges

- https://codeberg.org/ (libre)
- https://gittea.dev/
- https://gitlab.com/
- https://github.com/

- [Gitolite](https://gitolite.com/gitolite/index.html) - Gitolite vous permet de configurer l'hébergement git sur votre propre serveur, avec un contrôle d'accès précis et de nombreuses fonctionnalités plus puissantes.
  - [Explications complémentaires sur Gitolite](https://lycee.educinfo.org/index.php?page=gitolite&activite=git) (lycee.educinfo.org / Laurent COOPER)

### Tuto

- [Oh My Git!](https://ohmygit.org/) An open source game for Linux, Mac and Windows that builds intuition for Git! With real Git repositories.
- [Git au quotidien](https://diarra.notion.site/Git-au-quotidien-6494eebb3d4f4f6fbab46a8f3e0773aa) (un récap à moi)
- [Idiot proof git](https://softwaredoug.com/blog/2022/11/09/idiot-proof-git-aliases.html) - I’m an idiot. And git is hard. A lot of places use a rebase-based Git workflow, and I’ve made git less hard with a set of handy aliases.
- [oh shit git](https://ohshitgit.com/fr) *quelques exemples de mauvais pas où je me suis fourré, et comment j'ai réussi à m'en tirer, tout ça en français dans le texte*

## Markdown

- [Adam Pritchard's Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- [GitHub's Basic writing and formatting syntax (using Markdown)](https://docs.github.com/en/get-started/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)

- [Markdoc](https://markdoc.dev/) - Markdoc is an extension of Markdown but with extra features you’ll need when working with blog posts or documentation

- [Markwhen](https://markwhen.com/) - a Markdown based project planner with visual preview. Super nice tool for planning.

### Obsidian

[Obsidian](https://obsidian.md/) is great for creating clean documentation. Think about the easiness from [Notion](https://www.notion.so), but without the ads, the trackers and the your content getting takeovered by third parties.

Some interesting Obsidian plugins to mention :

- Emoji Magic (Community plugins) : easily add emoji with a powerfull keyword search.
- Unicode Search (Community plugins) : search and insert Unicode characters into your editor.
- Webpage HMTL Export (Community plugins) : export HTML from single files, canvas pages, or whole vaults. (some bugs, heavy pages, but nice rendering — editor's note)
- [Quartz](https://github.com/jackyzha0/quartz) : helps you publish your vault as a website for free.

## API

- [DummyJSON](https://dummyjson.com/) : Get dummy/fake JSON data to use as placeholder in development or in prototype testing
- [The Public APIs List](https://apislist.com/) : A community-curated and updated resource for public APIs.
- [Random User Generator API](https://randomuser.me/) : A free, open-source API for generating random user data. Like Lorem Ipsum, but for people.

## DNS

- [Julia Evans : Why is DNS still hard to learn?](https://jvns.ca/blog/2023/07/28/why-is-dns-still-hard-to-learn/) : *Julia Evans discusses the challenges of learning and troubleshooting DNS (Domain Name System) issues despite the apparent simplicity of the technology, exploring reasons such as hidden system complexities, confusing tools, weird gotchas, and infrequent exposure.*

## Hébergement

- https://codeberg.page/ (libre, gratuit, sans pub, sans tracker)
- https://pages.github.com/ (gratuit, mais avec trackers et [controverses](https://en.wikipedia.org/wiki/GitHub#Controversies))
- [Neocities.org](https://neocities.org/) - your own free website. Unlimited creativity, zero ads.
- https://byet.host/
- https://fr.000webhost.com/
- https://www.planethoster.com/fr/World-Lite
- https://www.infinityfree.net/

Hébergements expérimentaux :

- [smolsite.zip](https://smolsite.zip/UEsDBBQAAgAIAMYVIVfaTMb/VQIAANwDAAAKAAAAaW5kZXguaHRtbHVTUW+bMBB+Lr/ilq1i0wo4BGghJA/t9lBpk6Zpm6Y91cEOeAPMbCeEbv3vO0OjZdIK4mTuvvt8+vw5r0xTr5284pStc22Gmq+djWQD/HI2tPhRKrlrmVfIWqoMnm/HZ3laEg0teQY7Vb+cMWpoNiYCvS9fH5r64nxxg0voBTPVyp2HLlRclJXBdeLCXvD+Wh5WLgEC8xBsDrtavXIrY7osCPq+9/uFL1UZhIQQy+ueL94ibUdNBWzlvo/AT9MbjFEUQ4QxQrIYSOHHcWh5sRLbmKb7yCfhTQIxluIxJoB/kBTeBPbmHoJtTNMv+N03CVwVBKtRYpltHsbqE+QFAYv1LBhz3lj5H/mVZXdhK+p65Z6HC3bJYkanhCc7WggzoC5+9JhSu5qvXL7nrWTMDSYNrBy4mr1aOlvZGm9LG1EPGWjaak9zJfCsGqpK0WYo71V3WDoPTsFbwxUe8FlHGRNteVLrFLeFv+eLhx4S+y6ds6MLLol9lyf9IbHtZ4YfzNErU+rByYPJVXkweszJrbus4+brW+NqoKAbWWth+DOEzNeOk3frT5XQ0Fey5mArqIDRIFotGAdTcfj88R2iu0ewhEpqA4PcKY1O1BOmkE1D0aC1aNGf3c4AiqcGU+HEyIUbf7v9YLXlF3BNsSmJgLeFxC2EuQBsBYMy4A/QrRVsZi2p0ZPHgf170QWz7DiIwqvzAjAH3puv33+moGXDjzjckfGDb68bNEMtS+l3bYl4XlTyCeq7gpp/SX7DBifFQT2vV7QDcjdDgced82ASFhfTnXb+AFBLAQIeAxQAAgAIAMYVIVfaTMb/VQIAANwDAAAKAAAAAAAAAAEAAACkgQAAAABpbmRleC5odG1sUEsFBgAAAAABAAEAOAAAAH0CAAAAAA==) : Fits a full website (html, images, etc) in a zip, base64 encode it, and put it after `smolsite.zip` : your site is now hosted, only by its URL, ahah.

Hébergements payants :

- https://www.infomaniak.com/fr (au top)
- https://www.o2switch.fr/hebergement-illimite/
- https://www.ovhcloud.com/fr/web-hosting/personal-offer/ (service client déplorable)
- https://www.lafabriquedunet.fr/blog/comparatif-meilleurs-hebergeurs-web-ecologiques/ (article)

Démonstration de code source :

- [CodeSandbox](https://codesandbox.io/)
- [Sandpack](https://sandpack.codesandbox.io/) (une API proposée par le gens de [CodeSandbox](https://codesandbox.io/)) - voir aussi [la démo proposée par Joshua Comeau](https://www.joshwcomeau.com/react/next-level-playground/)
- [CodePen](https://codepen.io/)

## UI/UX

- [The history of user interfaces](https://history.user-interface.io/) : pour comprendre l'évolution des interfaces informatiques, depuis ses origines

## Couleurs

- [Color Formats in CSS](https://www.joshwcomeau.com/css/color-formats/) - Josh takes us on a tour of the slew of color formats at our disposal (hex, RGB, HSL, etc.), looking at how they work and how to get the best out of them. As with any of Josh's posts, there's a lot to dig into here.

### CSS color 4 specs

The new CSS Color 4 specification adds many better ways of declaring colors in CSS. Of these, oklch() is the most interesting one.

OKLCH is a new way to encode colors (like hex, RGBA, or HSL):

- OKLCH has native browser support.
- It can encode more colors for modern screens (P3, Rec. 2020, and beyond).
- Unlike HSL, OKLCH always has predictable contrast after color transformation.
- In contrast with LCH and Lab, no hue shift on chroma changes.
- Provides great a11y on palette generation.

- [OKLCH Color Picker & Converter](https://oklch.com/#65,0.284,17,100)
- [OKLCH in CSS: why we moved from RGB and HSL](https://evilmartians.com/chronicles/oklch-in-css-why-quit-rgb-hsl)
- [OKLCH Color Picker & Converter](https://oklch.evilmartians.io/) + [repo](https://github.com/evilmartians/oklch-picker)
- https://css.land/lch/
  - avec [cet article](https://lea.verou.me/2020/04/lch-colors-in-css-what-why-and-how/)
  - voir aussi [cette lib JS : color.js](https://lea.verou.me/2022/06/releasing-colorjs/), toujours par Lea Verou

### palette

- [Accessible color palette generator](https://venngage.com/tools/accessible-color-palette-generator) - Discover beautiful color combinations your whole audience can appreciate and follow Web Content Accessibility Guidelines (WCAG) with ease
- [Palette Generator](https://learnui.design/tools/data-color-picker.html)
- https://www.colourlovers.com/
- [Pppalette](https://fffuel.co/pppalette/) : An easy color palette generator for beautiful color schemes. Analogous, complementary, split complementary, triadic, monochromatic (tones, tints, shades) & more
- https://colorhunt.co/
- [Primer Prism](https://primer.style/prism/) - Github built a very nice app that lets us build custom color palettes
- https://paletton.com/
- https://coolors.co/palettes/trending
- https://color.adobe.com/fr/create/color-wheel
- https://www.randoma11y.com/ < nouvelle version <3
- https://reverb.wtf/
- [Alphredo.app](https://alphredo.app/) - How can transparent palettes change your UI designs
- [Leonardo](https://leonardocolor.io/): A One-of-a-Kind Tool for Creating, Managing, and Sharing Accessible Color Systems - From Adobe, this allows you to generate colors based on a desired contrast ratio, for better accessibility

### gradients

- [Mesher: A CSS Mesh Gradients Generator](https://csshero.org/mesher/) - tendance ! L'interface est très cool aussi.
- [uiGradients](https://uigradients.com/) - chouette collection et interface sympa
- [Highly Customizable Background Gradients](https://cloudfour.com/thinks/highly-customizable-background-gradients/)
- [Gradientify](https://gradientify.com/#try-gradientify) - 100+ Carefully Crafted Gradients ready to use in your next projects
- https://www.eggradients.com/

## Images et vidéos

### Wireframes / Diagrames / Schémas

- [Typograms](https://github.com/google/typograms/) : Turn ASCII art into diagrams, using JS. You can draw protocols, mocks, architecture, tables, flowcharts, trees, shapes, grids, time series, chips, circuits, mindmaps, scribbles, etc.
- https://excalidraw.com/ ([open source](https://github.com/excalidraw/excalidraw))
- https://www.tldraw.com/
- https://app.diagrams.net/ (draw.io) ([open source](https://github.com/jgraph/drawio))
- https://www.lucidchart.com/pages/
- https://www.gliffy.com/
- [LibreOffice Draw](https://www.libreoffice.org/discover/draw/)
- https://balsamiq.com/

### Maquette

- https://penpot.app/
- https://github.com/akiraux/Akira
- https://www.figma.com/ (racheté par Adobe)
- https://www.canva.com/
- https://icons8.com/lunacy
- https://pencil.evolus.vn/
- https://www.sketch.com/

### Illustrations

- [Streamline](https://www.streamlinehq.com/) - the world's largest  and most consistent icon and illustration sets
- [Vectormaker](https://vectormaker.co/) - Convert images to SVG vectors, PNG-to-SVG Vectors - Vectormaker outlines your pixel based images and turns them into colored SVG vector files.
- [Skribbl](https://weareskribbl.com/) - Beautiful, Hand-Drawn Illustrations
- [Boxy SVG Editor](https://boxy-svg.com/) Boxy SVG project goal is to create the best tool for editing SVG files online (très complet, pas de support FF pour le moment, hélas)
- [Piskel](https://www.piskelapp.com/) is a free online editor for animated sprites & pixel art : Create animations in your browser.
- [SVG Edit](https://github.com/SVG-Edit/svgedit) : SVGEdit is a fast, web-based, JavaScript-driven SVG drawing editor that works in any modern browser
- [Inkarnate](https://inkarnate.com/) : Create Fantasy Maps Online

### Illustrations / objets 3D

- [Hero Forge](https://www.heroforge.com/) : Create Fantasy 3D characters (interface web de ouf)
- [Blockbench](https://www.blockbench.net/) : An easy to use 3D model editor for low-poly and pixel-art.
- [Shapefest](https://www.shapefest.com/) : Objets modélisés en 3D - A massive library of free 3D objects
- [Kenney Assets](https://kenney.nl/assets) : dungeons, towns, planets, cars, etc.
- [Quaternius - Cube World Kit](https://quaternius.com/packs/cubeworldkit.html) : animated characters, animals, enemies and environmental models. In FBX, OBJ, glTF and Blend formats, free to use in personal and commercial projects.

### AI ART

- [Zoo](https://zoo.replicate.dev/?id=a-hyperrealistic-painting-of-ocean-waves-arts-and-crafts-movement-by-wu-shixian-onol40d) : comparing images from different AI models.
- [OpenArt](https://openart.ai/) : Get inspired from millions of amazing DALL·E 2 art and prompts
- [KREA](https://search.krea.ai/) : Inspiration from millions of prompts
- [r/dalle2](https://libreddit.kavin.rocks/r/dalle2/) : (alternative) Reddit chan updated with sample images from OpenAI's DALL·E 2
- [Opendream](https://github.com/varunshenoy/opendream) Web UI (running on Node) layering, non-destructive editing, portability, and easy-to-write extensions, to your *Stable Diffusion* workflows.

Et un article à contre-courant : https://alexanderwales.com/the-ai-art-apocalypse/

### Icônes

- [Tabler Icons](https://tabler.io/icons) - Pixel-perfect icons for web design *Free and open source icons designed to make your website or app attractive, visually consistent and simply beautiful*
- [Iconbuddy](https://iconbuddy.app/) - Download, Customize, Edit and Personalize.Over 180k+ open source icons
- [Mutant](https://mutant.tech/) - An experimental emoji set with new twists 💣
- [Streamline](https://www.streamlinehq.com/) - the world's largest  and most consistent icon and illustration sets
- [Feather Icons](https://feathericons.com/) - Simply beautiful open source icons
- [CSS icons](https://getcssscan.com/css-shapes) - Icons made with only CSS ? Yeah !
- [Icônes.js.org](https://icones.js.org/) - Icon Explorer with Instant searching .. hold on, it's got its [VSCode extension as well](https://github.com/afzalsayed96/vscode-icones) !
- [Simple Icons : SVG icons for popular brands](https://github.com/simple-icons/simple-icons) Over 2300 Free SVG icons for popular brands ([front here](https://simpleicons.org/))
- [Heroicons](https://heroicons.com/) - Beautiful hand-crafted SVG icons, by the makers of Tailwind CSS
- [Lucide](https://lucide.dev/) - Beautiful (elles aussi, lol) and consistent icon toolkit made by the community
- https://humbleicons.com/
- https://fluenticons.co/
- https://www.flaticon.com/
- https://game-icons.net/
- https://iconduck.com/
- https://www.svgrepo.com/

### Banques de photos et de vidéos

- https://openverse.org/ - 600 million Creative Commons / Public Domain works
- https://pxhere.com/ - Free of copyrights under CC0. Do whatever you want.
- https://www.pexels.com/
- https://unsplash.com/
- https://burst.shopify.com/
- https://pixabay.com/
- https://www.transhumans.xyz/ - open source illustrations of character transcending their biological barriers.

### Émojis

- https://getemoji.com/
- https://emojipedia.org/

### Images de placeholder

Et pour générer des images de test :
- https://picsum.photos/
- https://dummyimage.com/
- https://picsum.photos/
- https://source.unsplash.com/random/600x400

### Éditeurs d'image

- https://www.photopea.com/ : online photo editor (psd, ai, xd, fig, sketch, pdf, raw, etc.)
- https://www.vectorpea.com/ : edit right in the browser all major picture extensions : PSD, AI, XD, FIG, sketch, PDF, RAW, jpg, png, gif, tiff, svg.
- https://pixlr.com/fr/
- https://www.remove.bg/fr : pour retirer les backgrounds d'une photo, bluffant
- [Satori](https://github.com/vercel/satori) : Convert HTML and CSS to SVG
- [Brushify](https://brushify.art/create) : whimsical watercolors in seconds

### Image optimization

- https://www.smashingmagazine.com/2022/07/powerful-image-optimization-tools/ un bel article qui présente plein d'outils pour optimiser correctement vos images
- https://www.giftofspeed.com/jpg-compressor/
- https://jakearchibald.github.io/svgomg/
- https://www.simplified.guide/linux/compress-png-image-file
- https://www.giftofspeed.com/png-compressor/

## Audio

### Audio samples download

- https://samplefocus.com/
- https://freesound.org/
- https://www.zapsplat.com/
- https://lasonotheque.org/

### Audio tools

- [How to use Whisper](https://humanize.me/nerd/whisper.html) - tutorial en français sur l'utilisation du transcripteur libre et offline nommé "Whisper" et proposé par OpenAI
- [Dittytoy](https://dittytoy.net/) - Create your generative music online using a simple JavaScript API. Check some demos : [1](https://dittytoy.net/ditty/6f30b0885d) [2](https://dittytoy.net/ditty/b3585fb3a3) [3](https://dittytoy.net/ditty/827b1b3e63) [4](https://dittytoy.net/ditty/2a0acbb94b) 

## Video filters

- https://www.luthouse.com/free-luts

## Présenter son code, communiquer, faire des sliders, etc

Créer de belles captures d'écrans de son code :

- https://showcode.app/
- https://chalk.ist/
- https://carbon.now.sh/
- https://ray.so/
- ou, plus rapide ? directement depuis VSCode avec [l'extension CodeSnap](https://marketplace.visualstudio.com/items?itemName=adpyke.codesnap)
- https://recoded.netlify.app/  qui propose (aussi) l'export vidéo de son code

L'art de bien présenter :

> In business, communication needs to be clear and efficient. People are busy and don't have time to read long walls of text or listen to long presentations where the key info is shared at the very end.

- [Minto Pyramid](https://untools.co/minto-pyramid) : Make your communication more efficient and clear.

## Typographie

💡 Voir aussi la section [Web fonts](#web-fonts) de cette page.

- [Uncut.wtf](https://uncut.wtf/) : Download somewhat contemporary fonts, free for commercial use.
- [CopyChar.cc](https://copychar.cc/) : Nice big list of Unicode characters
- [Typography inspiration](https://shapeless.dev/foundations/typography) - Examples of font combinations used by my favorite websites
- [ZX Origins by DamienG](https://damieng.com/typography/zx-origins/) : I have designed many 8x8 bitmap fonts over the years. They are freely available.
- https://www.fontsquirrel.com/fonts/list/hot
- https://www.fontsquirrel.com/tools/webfont-generator
- https://www.dafont.com/fr/
- https://velvetyne.fr/ libre & open source type foundry
- https://fonts.google.com/ (illégal en Europe ? [article](https://javascript.plainenglish.io/how-google-fonts-became-illegal-in-europe-7d2b1575be01))
- [Atkinson Hyperlegible Font](https://brailleinstitute.org/freefont) : a font with greater legibility and readability for low vision readers

## HTML

- [Learn HTML5 and CSS3 From Scratch](https://invidious.fdn.fr/watch?v=mU6anWqZJcc) - Full Course (offered by freeCodeCamp.org)
- [Patrick Weaver's HTML Elements](https://www.patrickweaver.net/blog/a-blog-post-with-every-html-element/) - *Patrick Weaver shares his exploration and implementation of various HTML elements, including his experiences with less common elements, considerations for web accessibility, and insights gained from using different HTML tags in creating web content.*
- [The HTML CheetSheet](https://htmlcheatsheet.com/)
- https://validator.w3.org/ (code valide = la base)
- https://www.smashingmagazine.com/smashing-guide-search-engine-optimization/
- [HTML5 Test Page](https://github.com/cbracco/html5-test-page) : a test page filled with common HTML elements to be used to provide visual feedback
- [sémantique : article ou section](https://www.smashingmagazine.com/2022/07/article-section-elements-accessibility/) ?
- [Landmarks and where (not) to put them](https://www.htmhell.dev/adventcalendar/2022/4/) (HTMLHell) - overview of the landmark elements in HTML aside, footer, form, header, main, nav, section
- [internationalization](https://web.dev/learn/design/internationalization/) (`lang`, `hreflang`)
- [StatiCrypt](https://github.com/robinmoisson/staticrypt) - Encrypt a HTML page - StatiCrypt uses WebCrypto to generate a static, password protected page that can be decrypted in-browser. You can then just send or upload the generated page to a place serving static content (github pages, for example) and you're done: the page will prompt users for a password, and the javascript will decrypt and load your HTML, all done in the browser.

## Accessibilité

> To be fully accessible, a web page must be operable by someone using only a keyboard to access and control it. This includes users of screen readers, but can also include users who have trouble operating a pointing device such as a mouse or trackball, or whose mouse is not working at the moment, or who prefer to use a keyboard for input whenever possible. ([MDN](https://developer.mozilla.org/en-US/docs/Web/Accessibility/Understanding_WCAG/Keyboard))

- [Rule 1 : Don't use ARIA](https://web.dev/learn/accessibility/aria-html/) (web.dev)
- [Setting up a screen reader testing environment on your computer](https://www.sarasoueidan.com/blog/testing-environment-setup/) (Sara Soueidan) - *When you're designing and developing for accessibility, performing manual testing using a screen reader is important to catch and fix accessibility issues that cannot be caught by automated accessibility testing tools.*
- [Atkinson Hyperlegible Font](https://brailleinstitute.org/freefont) : a font with greater legibility and readability for low vision readers
- [Tota11y!](https://tota11y.babylontech.co.uk/) — A browser utility to visualize the most common web accessibility errors on a site in a clear, understandable way. (This builds upon [the original toolkit](https://khan.github.io/tota11y/) from Khan Academy)
- [Making Sense of WAI-ARIA](https://www.smashingmagazine.com/2022/09/wai-aria-guide/) - A Comprehensive Guide — An explainer looking at when to use ARIA and how to use it properly so that it’s helpful to the many people who use assistive technology to navigate the Internet
- https://www.w3.org/WAI/ARIA/apg/patterns/ (super bon récap)
- un article sur [l'utilité de :focus-visible](https://hidde.blog/focus-visible-more-than-keyboard/)
- Extension Figma : [Include](https://www.figma.com/community/plugin/1208180794570801545/Include%E2%80%94Accessibility-Annotations) (développée par les devs de chez eBay)
- Extension Chromium [Siteimprove Accessibility Checker](https://chrome.google.com/webstore/detail/siteimprove-accessibility/djcglbmbegflehmbfleechkjhmedcopn)
- Extension Chromium [Accessibility testing from TPGi](https://chrome.google.com/webstore/detail/arc-toolkit/chdkkkccnlfncngelccgbgfmjebmkmce)
- Extension Chromium [WCAG Color contrast checker](https://chrome.google.com/webstore/detail/wcag-color-contrast-check/plnahcmalebffmaghcpcmpaciebdhgdf)
- Extension [Lighthouse](https://chrome.google.com/webstore/detail/lighthouse/blipmdconlkpinefehnmjammfjpmpbjk/related?hl=fr) (déjà inclus dans le DevTools de Chromium)
- [AXE, "The Standard in Accessibility Testing"](https://www.deque.com/axe/)
- [Pa11y](https://github.com/pa11y/pa11y) : Pa11y is your automated accessibility testing pal. It runs accessibility tests on your pages via the command line or Node.js, so you can automate your testing process
- [Accessible Rich Internet Applications](https://www.w3.org/TR/wai-aria-1.2/) (WAI-ARIA) 1.2 (W3C)
- [Comprendre la norme WCAG](https://blogs.articulate.com/les-essentiels-du-elearning/comprendre-la-norme-wcag-guide-de-demarrage-rapide-pour-les-concepteurs-e-learning/)
- [Notices d'aide à l'accessibilité](https://www.accede-web.com/) "AcceDe Web"
- [Référentiel français de l’accessibilité](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/) (numerique.gouv.fr)

Mais aussi :

- Ne pas utiliser un lot de `<h1>` + `<h2>` + `<h3>` pour créer un bloc titre/sous-titres, ce type d'imbrication qui ne reflète pas ce qui est attendu. On passera plutôt par `<hgroup>`, comme démontré [dans cet excellent article](https://www.tpgi.com/subheadings-subtitles-alternative-titles-and-taglines-in-html/).

- [A Brief Introduction to JAWS, NVDA, and VoiceOver](https://css-tricks.com/comparing-jaws-nvda-and-voiceover/) - Article fouillé sur ces trois *screen readers*
- [WAVE Web Accesility Evaluation Tool](https://wave.webaim.org/) — WAVE is a suite of evaluation tools that helps authors make their web content more accessible to individuals with disabilities. WAVE can identify many accessibility and Web Content Accessibility Guideline (WCAG) errors, but also facilitates human evaluation of web content. Our philosophy is to focus on issues that we know impact end users, facilitate human evaluation, and to educate about web accessibility.
- (Windows) [NV Access](https://www.nvaccess.org/) — The NVDA screen reader can be downloaded free of charge by anyone. We do this because we believe everyone, especially the world’s poorest blind people deserve access to computers and a way out of poverty.
- (Apple) [VoiceOver](https://www.apple.com/accessibility/vision/) — VoiceOver is an industry‑leading screen reader that tells you exactly what’s happening on your device.
- (Linux) [Orca](https://doc.ubuntu-fr.org/orca) — Orca est un lecteur d'écran : il permet aux utilisateurs aveugles ou malvoyants d'utiliser pleinement les fonctionnalités d'un environnement de bureau GNU/Linux, grâce à une synthèse vocale, à un afficheur Braille ou à une loupe virtuelle, et à l'exploitation des outils d'accessibilité du système.
- [NerdeRegion](https://chrome.google.com/webstore/detail/nerderegion/lkcampbojgmgobcfinlkgkodlnlpjieb) — NerdeRegion is a Chrome extension for debugging live regions on a Web Page. When activated, it lists all active Aria live regions, and keeps a record of all mutations that has happened on the region.
- A11Y - Color blindness empathy test : [Chrome extension](https://chrome.google.com/webstore/detail/a11y-color-blindness-empa/idphhflanmeibmjgaciaadkmjebljhcc) + [Firefox add-on](https://addons.mozilla.org/en-US/firefox/addon/a11y-color-blindness-test/) — Empathy test for color blindness and visual impairment. This add-on emulates 8 types of color blindness, plus grayscale to check the contrast of your website.
- [Contrast Checker](https://contrastchecker.com/) — This tool is built for designers and developers to test color contrast compliance with the Web Content Accessibility Guidelines (WCAG) as set forth by the World Wide Web Consortium (W3C). These calculations are based on the formulas specified by the W3C.
- [Leonardo](https://leonardocolor.io/): A One-of-a-Kind Tool for Creating, Managing, and Sharing Accessible Color Systems - From Adobe, this allows you to generate colors based on a desired contrast ratio, for better accessibility
- Le rapport “accessibilité” de Lighthouse (Chrome DevTools > onglet Lighthouse) - [Lien vers la doc](https://developer.chrome.com/docs/devtools/?utm_source=devtools#accessibility)
- L’onglet “accessibilité” de Firefox (Firefox DevTools > onglet Accessibilité) - [Lien vers la doc](https://firefox-source-docs.mozilla.org/accessible/index.html)

Voir aussi les ressources dans [HTML](#html)

## SEO / Social markup

- https://meiert.com/en/blog/minimal-social-markup/

## Cartographie

Carte, Maps, etc.

- [Maptiler](https://www.maptiler.com) - Easy to use and beautiful (open street) maps, documentation, code samples, and
developer tools for web & mobile

## Environment

- [Web Sustainability Guidelines (WSG)](https://w3c.github.io/sustyweb/) (W3C)

## Node.js

- https://alexkondov.com/tao-of-node/ (Tao of Node - Design, Architecture & Best Practices)

## CSS

- [The CSS CheetSheet](https://htmlcheatsheet.com/css/)
- [CSS Reference](https://tympanus.net/codrops/css_reference/), by Codrops ❤️
- https://learn.shayhowe.com
- https://www.internetingishard.com
- https://developer.mozilla.org/fr/docs/Web/CSS/Reference
- https://www.youtube.com/c/LeDesignerduWeb/featured
- https://codepip.com/games/
- https://sparkbox.com/foundry/bem_by_example
- [Important properties of CSS](https://twitter.com/Prathkum/status/1401137125496602625) (twitter)
- [The box model concept](https://twitter.com/Prathkum/status/1393153623748747264) (twitter)
- [Web layout](https://twitter.com/Prathkum/status/1396444560402919427) (twitter)
- [CSS Positioning](https://twitter.com/Prathkum/status/1378618850888024067)
  - [Une petite démo explicite, et en français, de la propriété CSS `position:`](https://codepen.io/jeremiebold/pen/qBYXJGo) (codepen)
- [How to make responsive websites](https://twitter.com/Prathkum/status/1527005889961549828) (twitter)
- [z-index : a confusing concept of CSS](https://twitter.com/Prathkum/status/1414899812177088519) (twitter)
- [CSS filter methods : all you need to know about](https://twitter.com/Prathkum/status/1364142557052219394) (twitter)
- [The Box-shadow sweeeet gallery](https://getcssscan.com/css-box-shadow-examples)
- [Pokemon CSS](https://deck-24abcd.netlify.app/) Des cartes Pokémon en CSS, le résultat est carrément bluffant ! Le [repo est ici](https://github.com/simeydotme/pokemon-cards-css). *A collection of advanced CSS styles to create realistic-looking effects for the faces of Pokemon cards.*
- [Beautiful border radius - The Math Behind Nesting Rounded Corners](https://cloudfour.com/thinks/the-math-behind-nesting-rounded-corners/)
  - [CSS Clothoid Corners](https://onotakehiko.dev/clothoid/) - Generate clothoid rounded corners by CSS clip-path.
- [How to create slides with (only) HTML and CSS](https://www.silvestar.codes/articles/how-to-create-css-only-slides/)
- [Tree view in CSS](https://iamkate.com/code/tree-views/) - How to create a collapsible list (tree view) with just HTML and CSS ? no need for JavaScript !

[La chanson qui montre que "CSS is ok"](https://www.youtube.com/watch?v=lbqOCS9bMpk), ou presque, lol.

### Web fonts

Voir aussi la section [Typographie](#typographie) de cette page.

> On the web, the default font size is 16px. Some users never change that default, but many do. But by default, at least, 1em and 1rem will both be equal to 16px.

- https://learnui.design/tools/typography-tutorial.html (excellent tuto interactif)
- [Modern Font Stacks](https://modernfontstacks.com) - System font stack CSS organized by typeface classification for every modern OS. The fastest fonts available. No downloading, no layout shifts, no flashes — just instant renders.
- [Google Font Download](https://github.com/neverpanic/google-font-download) - a small shell script that allows you to download Google's web fonts to your local file system
- [Héberger soit-même des Google Fonts](https://gwfh.mranftl.com/fonts) (google webfonts helper)
- [The difference between `em` and `rem`](https://joshcollinsworth.com/blog/never-use-px-for-font-size), and why you should never use px to set font-size in CSS
- https://fonts.bunny.net/ (comme google fonts, mais respectueux de la vie privée, RGPD-friendly)
- https://systemfontstack.com/ (quelles sont les meilleurs fonts natives de chaque OS)
- [Fallback Font Generator](https://screenspan.net/fallback) - Reduce Cumulative Layout Shift (CLS) by adjusting web fonts and system font fallbacks using special @font-face descriptors
- [Advanced web font optimization techniques](https://dev.to/alex_barashkov/advanced-web-font-optimization-techniques-2n1f)

### Media queries

- [A Practical Guide to CSS Media Queries](https://stackdiary.com/css-media-queries/)
- [A quick beginner's guide to CSS Media Queries](https://twitter.com/Prathkum/status/1363063942994685952) (twitter)
- [What was that media query code again??](https://mediaquery.style/) A website that lists popular media queries with a handy copy button.

### Container queries

- [Container Queries: Style Queries](https://www.bram.us/2022/10/14/container-queries-style-queries/) - Learn what it means that CSS Container Queries are more than “check the size of a container”

### Unités

- [New Viewport Units](https://12daysofweb.dev/2022/new-viewport-units/) - Review newly available units to manage space within the large, small, and dynamic viewport spaces.
  - Voir aussi [Large, Small, and Dynamic viewport units](https://caniuse.com/viewport-unit-variants) (Can I use)
- [Exemple d'utilisation de `clamp()`](https://youtu.be/tueTFd2TQUA?t=1852) (extrait vidéo)
- [min(), max(), and clamp()](https://web.dev/min-max-clamp/) - three logical CSS functions to use today
- [auto-fill, auto-fit, minmax()](https://ishadeed.com/article/css-grid-minmax/)

### Sélecteurs

- [The Difference Between ID and Class](https://css-tricks.com/the-difference-between-id-and-class/)
- [Multiple Class / ID and Class Selectors](https://css-tricks.com/multiple-class-id-selectors/)
- [Beginner Concepts: How CSS Selectors Work](https://css-tricks.com/how-css-selectors-work/)
- [CSS Selectors: A Visual Guide](https://fffuel.co/css-selectors/) : Here's a visual guide to the most popular CSS selectors
- [Combinators](https://tympanus.net/codrops/css_reference/combinators/)
- Entraînement ludique Sélecteurs CSS : [CSS Diner](https://flukeout.github.io/)
- Comprendre [nth-child](https://css-tricks.com/almanac/selectors/n/nth-child/) et autre [nth-of-type](https://css-tricks.com/almanac/selectors/n/nth-of-type/) :
  - https://css-tricks.com/useful-nth-child-recipies/
  - https://css-tricks.com/almanac/selectors/n/nth-child/
  - https://css-tricks.com/examples/nth-child-tester/#
- [Attribute Selectors](https://tympanus.net/codrops/css_reference/attribute-selectors/)
- The `:has()` pseudo-class 
  - [The `:has()` pseudo-class](https://www.youtube.com/watch?v=OGJvhpoE8b4) is the "parent selector". But it can actually do a lot more than that (Kevin Powell YT) - avec un rappel rigolo sur le fait que non, on ne peut pas avoir plusieurs `h1` sur une page
  - [The CSS :has() selector is way more than a “Parent Selector”](https://www.bram.us/2021/12/21/the-css-has-selector-is-way-more-than-a-parent-selector/)
  - [`:has()` is here and you probably need it in your CSS](https://www.bekk.christmas/post/2022/8/has-is-here-and-you-probably-need-it-in-your-css) - it is time to learn what it is, why you should care and how you use it.

### Masonry

- https://prototypr.io/post/masonry-layout-css-tailwind (masonry, cette mise en page façon Pinterest)
- https://css-irl.info/masonry-in-css/
- [a 100% CSS masonry layout—no JavaScript necessary at all!](https://webdesign.tutsplus.com/tutorials/masonry-layouts-with-css-grid-and-object-fit-cover--cms-37989)
- a masonry, using CSS columns :
  - https://codepen.io/geoffgraham/pen/qBoQOWd
  - https://codepen.io/AdamBlum/full/DZXELw

### Aspect ratio

- [aspect ratio is really useful](https://youtu.be/tueTFd2TQUA?t=837) (extrait vidéo)

### Flexbox

- [An Interactive Guide to Flexbox](https://www.joshwcomeau.com/css/interactive-guide-to-flexbox/) sublime guide interactif. Vraiment génial.
- [Everything you need to know about CSS Flex layout](https://twitter.com/Prathkum/status/1379881164912099334) (twitter)
- La bible concernant Flex : https://css-tricks.com/snippets/css/a-guide-to-flexbox/
- [Flexbox playground (codepen)](https://codepen.io/enxaneta/pen/adLPwv) : interactif, très visuel
- Entraînements ludiques Flexbox : http://www.flexboxdefense.com/ et https://flexboxfroggy.com/#fr
- [Flexbox ou Grid](https://youtu.be/tueTFd2TQUA?t=574) (extrait vidéo)
- [Flexbox Playground (flexbox.tech)](https://flexbox.tech/) pas mal

### Grid

- La bible concernant Grid : https://css-tricks.com/snippets/css/complete-guide-grid/
- [Grid by Example](https://gridbyexample.com/examples/) - Usage examples of CSS Grid Layout
- [CSS Grid is tricky](https://twitter.com/Prathkum/status/1518162718213160960) (twitter)
- [auto-fill, auto-fit, minmax() - A Deep Dive Into CSS Grid](https://ishadeed.com/article/css-grid-minmax/)
- [Explications sur les container Queries](https://youtu.be/tueTFd2TQUA?t=1471) (extrait vidéo)
- [Explications sur l'utilisation de Subgrid](https://youtu.be/tueTFd2TQUA?t=2267) (extrait vidéo)
- Entraînement ludique Grid : http://cssgridgarden.com/
- [CSS Grid Generator](https://cssgrid-generator.netlify.app/)

### Pseudo-classes

- [Utilité de la pseudo-classe :has()](https://www.youtube.com/watch?v=axkefJvfS9U) (vidéo d'Eric Meyer)
- [Utilité de la pseudo-classe :has()](https://youtu.be/tueTFd2TQUA?t=2470) (vidéo de Michelle Barker)

### CSS Resets and Normalizers

> *Resets remove defaults to achieve a more ‘blank canvas’*
> 
> *Normalizers attempt to make the defaults more consistent across browsers*

- https://codeberg.org/jrm-omg/jerry-custom-css-reset/
- [58 bytes of CSS to look great nearly everywhere](https://gist.github.com/JoeyBurzynski/617fb6201335779f8424ad9528b72c41)
- https://www.miriamsuzanne.com/2019/11/02/most-normal/
- https://github.com/necolas/normalize.css
- https://github.com/jensimmons/cssremedy
- https://github.com/jgthms/minireset.css
- https://csstools.github.io/normalize.css
- https://csstools.github.io/sanitize.css
- https://piccalil.li/blog/a-modern-css-reset/
- https://github.com/sindresorhus/modern-normalize
- https://unpkg.com/tailwindcss@3.1.4/src/css/preflight.css
- https://www.joshwcomeau.com/css/custom-css-reset/
- [Elly Loel's CSS Reset](https://gist.github.com/EllyLoel/4ff8a6472247e6dd2315fd4038926522)
- https://github.com/elad2412/the-new-css-reset

### CSS masks

- [CSS Masking](https://ishadeed.com/article/css-masking/) (Ahmad Shadeed)

### HTML test

- https://github.com/cbracco/html5-test-page : if you wanna test any CSS frameworks or resets, normalizes against a test page, this is the one you need

### CSS frameworks

- https://picocss.com (semantic first!)
- [MVP.css](https://andybrewer.github.io/mvp/) (no classes)
- https://tailwindcss.com
- https://www.cirrus-ui.com
- https://bulma.io
- https://shoelace.style
- https://getbootstrap.com
  - [themes for Bootstrap](https://bootswatch.com/)
  - variante de Bootstrap : https://www.gethalfmoon.com
- https://purecss.io
- [Primer CSS](https://github.com/primer/css) - The CSS design system that powers GitHub
- https://picnicss.com
- http://getskeleton.com
- https://milligram.io
- https://www.getpapercss.com
- https://nostalgic-css.github.io/NES.css/ (NES, yeah)
- https://terminalcss.xyz (Terminal, yeah)
- https://jdan.github.io/98.css/ (Win 98, yeah)
- [a2000 (a2k)](https://a2000-docs.netlify.app/) - Capture the feel of the early web using modern web tools and practices (Win XP, yeah)
- https://get.foundation
- https://semantic-ui.com
- https://www.mockplus.com/blog/post/css-framework (article sur les frameworks)
- [system.css: A Design System for Building Retro Apple Interfaces](https://sakofchit.github.io/system.css/)

Voir également [ce gros topic CSS framework](https://github.com/topics/css-framework), sur GitHub

### CSS animations

- [Easing Equations](https://gizma.com/easing/) (JavaScript but also CSS beautiful formulas)
- [Introduction to CSS Animation](https://twitter.com/Prathkum/status/1397170612183912450) (twitter)
- [Introduction to CSS Transition](https://twitter.com/Prathkum/status/1398027987929419785) (twitter)
- https://joshcollinsworth.com/blog/great-transitions - Ten tips for better CSS transitions and animations. Vraiment cool, pertinent et bien expliqué.
- https://joshcollinsworth.com/blog/easing-curves - Understanding easing and cubic-bezier curves in CSS. Pour tout comprendre des courbes cubic-bezier en CSS
- [Animating Grid](https://invidious.fdn.fr/watch?v=DxJXvTFiWSI) ? Yes ! Thanks to this Wes Bos' video
- https://7ph.github.io/powerglitch/#/ - PowerGlitch is a standalone library with no external dependencies. It leverages CSS animations to create a glitch effect on images. No canvas or DOM manipulations are needed.
- https://github.com/nateplusplus/pushin
- https://olivier3lanc.github.io/Scroll-Btween/
- https://animista.net/play/basic
- https://animate.style/
- http://bouncejs.com/
- https://kristofferandreasen.github.io/wickedCSS/
- https://shakrmedia.github.io/tuesday/
- https://www.minimamente.com/project/magic/
- https://ianlunn.github.io/Hover/
- https://www.imaginarycloud.com/blog/how-to-make-css-animations/
- [un effet de parallax rigolo](https://atroposjs.com/) pour mettre en avant des images (pochettes d'albums, boîtes de jeux, etc.)

- View Transitions API (c'est pour bientôt)
  - [View Transitions API](https://12daysofweb.dev/2022/view-transitions-api/) - Providing a native API for animated page and element transitions.

### CSS animations via JS

- [Shifty](https://jeremyckahn.github.io/shifty/doc/) - The fastest JavaScript animation engine on the web
- [Scene.js](https://github.com/daybrush/scenejs) : a JavaScript & CSS timeline-based animation library ([demos](https://codepen.io/collection/DLWxrd/))
- https://auto-animate.formkit.com/
- https://animejs.com/
- https://christinecha.github.io/choreographer-js/
- https://anijs.github.io/
- https://github.com/aholachek/animate-css-grid
- https://kenwheeler.github.io/slick/ (slider)
- https://maxwellito.github.io/vivus/ (SVG)
- https://michalsnik.github.io/aos/ (scroll)

### Sass

- https://blog.mayank.co/the-case-for-using-sass-in-2022
- https://codeberg.org/jrm-omg/sass-boilerplate

## JavaScript

- Charger des JS : [Standard vs `async` vs `defer` vs `async defer`](https://gist.github.com/jakub-g/385ee6b41085303a53ad92c7c8afd7a6)
- [Bien comprendre la différence entre `DOMContentLoaded` et `load`](https://fr.javascript.info/onload-ondomcontentloaded) (ainsi que `beforeunload`/`unload`)
- [Comment (bien) utiliser les fonctions flêchées de JS](https://pixelprincess.fr/les-fonctions-flechees-en-javascript-moderne/) ? (pixelprincess)
- [Carousels (sliders) stylés et rapides](https://www.embla-carousel.com/examples/basic/)
- [Driver.js](https://driverjs.com/) : Product tours, highlights, contextual help and more. (Une sorte de "Wizard")
- [Utilité de la balise `<template>`](https://kittygiraudel.com/2022/09/30/templating-in-html/#why-not-a-hidden-element)
- [Processing Arrays non-destructively: `for-of` vs. `.reduce()` vs. `.flatMap()`](https://2ality.com/2022/05/processing-arrays-non-destructively.html)

### Easings

- [Easing Equations](https://gizma.com/easing/) (JavaScript but also CSS beautiful formulas)

### Mathematics

- [KaTeX](https://katex.org/) : a fast math renders that doesn’t need to reflow the page. KaTeX’s layout is based on Donald Knuth’s TeX, "the gold standard for math typesetting".

### Datavis

- [Observable Plot](https://observablehq.com/plot/features/marks) - Create expressive charts with concise code

### Console.log

- [Eruda](https://eruda.liriliri.io/) : A console for the mobile browser !!

### Asynchrone

- [(How to) retry XMLHttpRequest Carefully](http://lofi.limo/blog/retry-xmlhttprequest-carefully)
- https://github.com/jcubic/sysend.js (messages entre différents tabs d'un même browser)
- [XMLHttpRequest, callbacks and promises](https://alicemoretti.medium.com/xmlhttprequest-callbacks-and-promises-257a4e63fe9a) - très cool article, avec des exemples de XHR clairs, ce qui est rare
- [ofetch](https://unjs.io/ofetch) - A better fetch API. Works on node, browser and workers. (from [unjs](https://unjs.io/), the "Unified JavaScript Tools")

### Bookmarklet

Un bookmarklet est un petit morceau de code JavaScript pouvant être stocké en tant qu'URL dans un marque-page (lien favori) d'un navigateur Web. On peut donc créer des petits scripts JS, exécutable d'un simple click, et qui vont pouvoir impacter n'importe quel site. *Exemples : modifier le rendu graphique d'une page web, récupérer des informations d'une page web, soumettre la page courante à un service web tiers, etc.*

- [Make Bookmarklets](https://make-bookmarklets.com/)

➡️ Et pour automatiser le lancement d'un script JS personnalisé, sur une page donnée, on peut passer par des userscripts, voir la section "User scripts" juste en dessous.

### Frameworks

- [Strawberry](https://18alan.space/strawberry/) : Strawberry is a tiny frontend framework that gives you reactivity and composability. It does this with zero-dependencies, without a build step, and it fits in less than 3KB when gzipped.

### User scripts

Pimp *ANY* website with *YOUR* JS scripts !

> User scripts put you in control of your browsing experience. Once installed, they automatically make the sites you visit better by adding features, making them easier to use, or taking out the annoying bits.

- [Greasy Fork](https://greasyfork.org/en) ([en Français](https://greasyfork.org/fr) aussi) : great howto, a nice « quick start » on the user scripts world !
- Violentmonkey ([Firefox](https://addons.mozilla.org/firefox/addon/violentmonkey/) [Chrome](https://chrome.google.com/webstore/detail/violent-monkey/jinjaccalgkegednnccohejagnlnfdag))
- Greasemonkey ([Firefox](https://addons.mozilla.org/firefox/addon/greasemonkey/))
- Tampermonkey ([Firefox](https://addons.mozilla.org/firefox/addon/tampermonkey/) [Chrome](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo))
- [Fix your life with Greasemonkey Scripts](https://invidious.fdn.fr/watch?v=6DmQ_V9ZRlk) (Wes Bos video)

### JS games

Améliorer son niveau, en codant des jeux :

- [Leek Wars](https://leekwars.com/) - Un jeu de programmation dans lequel vous devez créér le plus puissant poireau et détruire vos ennemis
- [CodinGame](https://www.codingame.com/) - permettre aux développeurs d'améliorer leurs compétences en continu en résolvant les problèmes de code les plus motivants et en échangeant avec les meilleurs programmeurs du monde
- [Codewars](https://www.codewars.com/) - Improve your development skills by training with your peers on code kata that continuously challenge and push your coding practice

Et quelques jeux existants, pour l'inspiration :

- [Norman the Necromancer](https://js13kgames.com/entries/norman-the-necromancer)
- [js13kGames 2022 winners 🏆](https://github.blog/2022-10-06-js13k-2022-winners/)

### JS scripts

- [WinBox.js](https://github.com/nextapps-de/winbox) : A modern window manager for the web: lightweight, outstanding performance, no dependencies, fully customizable, open source
- [TweakPane](https://cocopon.github.io/tweakpane/) : Compact pane library for fine-tuning parameters and monitoring value changes
- [Lenis](https://lenis.studiofreight.com/) - smooth scroll or die

### Coloration syntaxique de code, via JS

- [Highlight.js](https://highlightjs.org/) (coloration seule) : Syntax highlighting for the Web
- [Prism.js](https://prismjs.com/) (coloration seule) is a lightweight, extensible syntax highlighter + [live edition demo](https://rety.verou.me/)
- [CodeMirror](https://codemirror.net/) (coloration ET édition) is a **code editor** component for the web

## Task runner / assets bundler

- https://lightningcss.dev/
- https://gruntjs.com/
- https://parceljs.org/
- https://brunch.io/
- https://esbuild.github.io/
- https://webpack.js.org/
- https://rollupjs.org/guide/en/
- https://vitejs.dev/
- https://www.snowpack.dev/

## PHP

- [Kint](https://github.com/kint-php/kint) - un `var_dump()`, en mille fois plus pratique (debugging helper for PHP developers)
- [Mecha](https://github.com/mecha-cms/mecha) : a flat-file content management system for minimalists "The world has been filled with social media services that are no longer safe to share personal matters. I want to grow back the interest of future generations to write journals on the internet. Mecha is a flat-file content management system that carries the concept of minimalism. This content management system ensures that you have full control over the content you have."
- [ReactPHP](https://reactphp.org/) Event-driven, non-blocking I/O with PHP : Its event-driven architecture makes it a perfect fit for efficient network servers and clients handling **hundreds or thousands** of concurrent **connections**, long-running applications and many other forms of cooperative multitasking with non-blocking I/O operations.

### PHP + MySQL

- [MeekroDB](https://meekro.com/) - The Simple PHP MySQL Library

## Database

- [Learn SQL](https://www.sql-practice.com/) (sql-practice.com) - practice SQL online with this great playground (easy, medium and hard questions)

## Mobile development and PWA

PWAs can be accessed through a web browser, but they can also be installed on a user's home screen...

- [Provide PWA an installable experience](https://web.dev/progressive-web-apps/#provide-an-installable-experience) (Thomas Steiner)
- [PWABuilder](https://web.dev/pwas-in-app-stores/) is a powerful tool that allows developers to create packages that can be submitted to various app stores : Google Play Store, Microsoft Store, Apple App Store, Meta Quest Store
- [Framework7](https://framework7.io/) - is a free and open source framework to develop mobile, desktop or web apps with native look and feel. It is also an indispensable prototyping tool to show working app prototype as soon as possible in case you need to.

## Webassembly

- [How WebAssembly is accelerating new web functionality](https://blog.chromium.org/2023/04/how-webassembly-is-accelerating-new-web.html) (Chromium Blog)
- [Moonbit](https://moonbitlang.com/blog/first-announce/) - the fast, compact & user friendly language for WebAssembly

## Python

- [Lumi](https://github.com/Lumi-Official/lumi) - a nano framework to convert your python functions into a REST API without any extra headache.
- [PyFlo](https://pyflo.net/) - The beginners guide to becoming a Python programmer

## Sécurité

- [Always avoid using the `chmod 777` command](https://pimylifeup.com/chmod-777/) - In this guide, we will explain to you what `chmod 777` does and why setting that permission should rarely be done.

- [MOOC de l'ANSSI](https://secnumacademie.gouv.fr/) : formez-vous à la sécurité du numérique - s'initier à la cybersécurité, approfondir vos connaissances, et ainsi agir efficacement sur la protection de vos outils numériques ([article LinuxFR](https://linuxfr.org/users/gbetous/journaux/j-ai-fait-le-mooc-de-l-anssi))
- https://github.com/swisskyrepo/PayloadsAllTheThings : A list of useful payloads and bypass for Web Application Security and Pentest/CTF
- https://book.hacktricks.xyz/welcome/readme : (beautiful) hacking trick/technique/whatever
- https://github.com/vavkamil/awesome-bugbounty-tools : A curated list of various bug bounty tools
- https://github.com/toniblyx/my-arsenal-of-aws-security-tools : List of open source tools for AWS security: defensive, offensive, auditing, DFIR, etc
- https://github.com/vitalysim/Awesome-Hacking-Resources : A collection of hacking / penetration testing resources to make you better!
- https://github.com/dustyfresh/PHP-vulnerability-audit-cheatsheet : Find potentially vulnerable PHP code.
- https://github.com/enaqx/awesome-pentest : A collection of awesome penetration testing resources, tools and other shiny things 
- https://github.com/edoardottt/awesome-hacker-search-engines : A curated list of awesome search engines useful during Penetration testing, Vulnerability assessments
- https://github.com/payloadbox/xss-payload-list : Cross Site Scripting ( XSS ) Vulnerability Payload List
- https://www.hacksplaining.com/ : plein de failles expliquées et testables (comme sur cette [démo ici](https://www.hacksplaining.com/exercises/sql-injection#/first-login-attempt))

## JSON

- [MistQL](https://github.com/evinism/mistql) - a query language for JSON-like structures (cross-platform : Python, JS)
- [JSON Hero](https://jsonhero.io/) - A Beautiful JSON Viewer — Paste in some JSON or enter a URL to a JSON file and this tool provides a clean and beautiful UI packed with features.
- [JSON Crack](https://jsoncrack.com/) - Seamlessly visualize your JSON data instantly into graphs

## CMS

- [Eleventy](https://www.11ty.dev/docs/), un générateur de site à partir de fichiers `.md`, `.html` et [autres](https://www.11ty.dev/docs/languages/)
- [Jigsaw](https://jigsaw.tighten.com/) : a static site generator built by Laravel partners
- [Drupal](https://www.drupal.org/)
- [SPIP](https://www.spip.net/)
- [Joomla](https://www.joomla.fr/)
- [PrestaShop](https://www.prestashop.com/fr), e-commerce
- [Magento](https://magento.com/), e-commerce
- [Jekyll](https://jekyllrb.com/)
- [Hugo](https://gohugo.io/), static site generator
- [Kirby](https://getkirby.com/)
- [Grav](https://getgrav.org/), “zero installation”
- [WordPress](https://fr.wordpress.org/download/)
  - [Faster WordPress rendering with 3 lines of configuration](https://www.phpied.com/faster-wordpress-rendering-with-3-lines-of-configuration/)
- [markPush](https://codeberg.org/jrm-omg/markpush) : convertir des fichiers markdown en HTML
- [Une grosse liste de "static web site generators"](https://github.com/myles/awesome-static-generators)

## No-code

- [Popsy](https://popsy.co/) : A no-code website builder that works like Notion where you can customize a design and publish.
- [Alternatives](https://shuffle.dev/alternatives) (shuffle.dev) : "We re-imagined how to build websites visually." A drag & drop website builder that returns multiple pages and styles at once.

### e-commerce

- [Easyful](https://www.easyful.com/) - selling online with zero fees & zero code ; All-in-one Solution : turn your Stripe Dashboard into your single source of truth. Manage products, view customers, orders, and reports right from Stripe.

## Kanban

- [Nullboard](https://github.com/apankrat/nullboard) is a minimalist (single file) kanban board, focused on compactness and readability. Can be used completely offline. All data is stored locally, for now using localStorage. Can be exported to- or imported from a plain text file in a simple JSON format.

## Linux

- [Dompter le Shell](https://linuxjourney.com/lesson/the-shell) (exercices interactifs)
- [OpenSnitch](https://github.com/evilsocket/opensnitch) is a GNU/Linux application firewall
- [RustDesk](https://rustdesk.com/) - Un TeamViewer libre et gratuit

## Jeux

- [TaleSpire](https://talespire.com/) : Animez vos Jeux de Rôles dans ce simulateur online. Import de figurines [Hero Forge](https://www.heroforge.com/) possible, fiches persos, etc.

## Électronique

- [Circuit.js](http://lushprojects.com/circuitjs/circuitjs.html) – electronic circuit simulator on the web (bluffant, avec les osciloscopes et tout)

## Plaisir des yeux

- [Optical Illusions](https://optical.toys/) : une superbe série d'illusions. Bluffant ! 
- [Puter](https://puter.com/) : un OS dans le navigateur - a cloud operating system. Store, open, and edit your files from anywhere at any time in the cloud
- Matrix !!! [démo](https://rezmason.github.io/matrix/) - [repo](https://github.com/Rezmason/matrix)
- [Windows 95](https://codepen.io/jkantner/full/oNypPOZ)
- [no-ht.ml](https://no-ht.ml/)
- [Love Will Tear Us, Again](https://pouria.dev/unknown-pleasures)

## Éducation au numérique

- [Pouvez-vous détecter une arnaque ?](https://ici.radio-canada.ca/info/decrypteurs/quiz-arnaques-fraude/) Quizz intéractif et pédagogique orienté phishing, proposé par radio-canada.

## Misc

- Read later [SingleFile](https://github.com/gildas-lormeau/SingleFile) : save a complete web page into a single HTML file
- [PDF web tools](https://www.pdftool.org/en) modify your PDF files in your browser, no upload required (encrypt, decrypt, optimize, merge, rotate, split, sign)
- [Lemmy](https://github.com/LemmyNet/lemmy), a Reddit alternative : Lemmy est une plateforme libre (AGPL) et fédérée. ([article LinuxFr.org](https://linuxfr.org/news/lemmy-une-alternative-libre-a-reddit))
- [Radio-Browser](https://www.radio-browser.info/countries) : Listen to any radio, worldwide (API available)
